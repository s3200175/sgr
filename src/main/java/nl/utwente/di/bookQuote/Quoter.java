package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    public Map<String,Double> BookAndPrice = new HashMap<>();

    public Quoter(){
        BookAndPrice.put("1" , 10.0);
        BookAndPrice.put("2" , 45.0);
        BookAndPrice.put("3" , 20.0);
        BookAndPrice.put("4" , 35.0);
        BookAndPrice.put("5" , 50.0);
        BookAndPrice.put("others" , 0.0);

    }
    public double getBookPrice(String ronaldo) {
        return BookAndPrice.get(ronaldo);
    }

}
